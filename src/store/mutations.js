// import * as types from './mutation-types'

// export default {
//   [types.UPDATE_FOO] (state, payload) {
//     state.foo = payload
//   }
// }
const getDomainName = (urlStr) => {
    const preDomain = ((urlStr).match(/^(?:https?:\/\/)?(?:[^@\n]+@)?(?:www\.)?([^:\/\n?]+)/img)[0] || '').toString().toLowerCase();
    return preDomain.split('\/\/')[1] || preDomain.split('\/\/')[0];
};
import {
    groupBy
} from 'lodash'
export const mutations = {
    mUpdateTabs(state, srcTabs) {
        const inputNewTabs = srcTabs.map(v => {
            return {
                url: v.url,
                domain: getDomainName(v.url),
                tid: v.id,
                wid: v.windowId,
                title: v.title,
                icon: v.favIconUrl || '',
                pinned: v.pinned,
                active: v.active,
                audible: v.audible,
                highlighted: v.highlighted,
                status: v.status,
                index: v.index,
                // debug: v,
            }
        });
        state.tabs = inputNewTabs;
        state.domains = groupBy(inputNewTabs, 'domain');
        // const domns = groupBy(state.tabs, 'domain');
        // state.domains = map(Object.keys(domns).sort().reduce((accumulator, currentValue) => {
        // 	accumulator[currentValue] = domns[currentValue];
        // 	return accumulator;
        // }, {}));
        // console.log('mutation state.tabs:', state.tabs);
        // console.log('mutation state.domains:', state.domains);
    },
    mUpdateTemp(state, param) {
        state.temp = param;
        // console.log('mutation mUpdateTemp:', state.temp);
    },
    mUpdateFilter(state, text) {
        state.filter = String(text).trim();
        // console.log('mutation state.filter:', state.filter);
    },
    mUpdateHistory(state, data) {
        // state.history = data;
        state.history = data.map(v => {
            return {
                url: v.url,
                domain: getDomainName(v.url),
                tid: +v.id,
                title: v.title,
                lastVisitTime: v.lastVisitTime,
            }
        });
        // console.log('mutation state.history:', state.history);
    },
}