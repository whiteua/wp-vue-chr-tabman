// import * as types from './mutation-types'
// export const setFoo = ({commit}, payload) => {
//   commit(types.UPDATE_FOO, payload)
// }

// import _ from 'lodash'
// import {groupBy} from 'lodash'
export const actions = {
    aFetchAllTabs(ctx) {
        return new Promise((resolve)=>{
            chrome.tabs.query({}, res => {
                ctx.commit('mUpdateTabs', res);
                resolve(res);
            });
        });
    },
    aRemoveTabs(ctx, prm) {
        // HSCK "Vuex getter not updating"
        // https://stackoverflow.com/questions/40860592/vuex-getter-not-updating
        // https://vuejs.org/v2/guide/reactivity.html#Change-Detection-Caveats
        return new Promise((resolve)=>{
            chrome.tabs.remove(prm.idxs, async () => {
                if (prm.oneIdx) {
                    ctx.state.domains[prm.domain].splice(prm.oneIdx, 1);
                    resolve(true);
                    return;
                }
                chrome.tabs.query({}, res => {
                    ctx.commit('mUpdateTabs', res);
                    resolve(true);
                });
            });
        });
    },
    aFetchHistory({ commit, state }) {
        // const flt = [...state.filter];
        // console.log('actions filter:', flt, typeof flt);
        // console.log('actions state.filter:', state.filter, typeof state.filter);

        chrome.history.search({
            text: state.filter,
            maxResults: 20
        }, data => {
            commit('mUpdateHistory', data);
        });
    },
}