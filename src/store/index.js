import Vue from 'vue'
import Vuex from 'vuex'

import {getters} from './getters'
import {mutations} from './mutations'
import {actions} from './actions'

console.log('getters:', getters)

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    tabs: [],
    domains: [],
    history: [],
    filter: '',
    temp: undefined,
  },
  getters,
  mutations,
  actions
})
