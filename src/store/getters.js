export const getters = {
    getAllTabs: state => state.tabs,
    getDomains: state => state.domains,
    getHistory: state => state.history,
    getAudibles: state => state.tabs.filter(v => v.audible),
    getPinneds: state => state.tabs.filter(v => v.pinned),
    getDomainsFiltered: state => {
        const filterObject = (inputObj, PATTERN)=>{
            try {
              return Object.keys(inputObj)
                .filter(key => Object.keys(inputObj).filter( str=>{return str.includes(PATTERN)}).includes(key))
                .reduce((obj, key) => {
                  obj[key] = inputObj[key];
                  return obj;
                }, {});
            } catch (e) {
              return {};
            }
        }
        return (state.filter)
            ? filterObject(state.domains, state.filter)
            : state.domains
        ;
    },
}
