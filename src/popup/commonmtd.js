export const cmn = {
    gotoTab(tabId, windowId) {
        try {
            chrome.tabs.update(tabId, {
                selected: true
            });
            chrome.windows.update(windowId, {
                focused: true
            });
        } catch (error) {

        }
    },
    getAllTabs(query={}) {
        return new Promise((resolve, reject)=>{
            chrome.tabs.query(query, res => {
                resolve(res)
            })
        });
    },
    getTab(tabId) {
        return new Promise((resolve)=>{
            try {
                chrome.tabs.get(tabId, tabInfo=>{
                    resolve(tabInfo)
                })
            } catch (error) {
                resolve(false)
            }
        });
    },

    // getHistory(tabId, windowId){
    //   try {
    //       chrome.tabs.update(tabId, {selected:true});
    //       chrome.windows.update(windowId, {focused:true});
    //   } catch (error) {

    //   }
    // },
    // async remove_Tabs_1(tabsArr){
    //   try {
    //     chrome.tabs.remove(tabsArr);
    //   } catch (error) {

    //   }
    // },
    async createUrlInNewTab(url, tabId) {
        try {
            const tabInfo = await this.getTab(tabId);
            if (tabInfo) {
                this.gotoTab(tabInfo.id, tabInfo.windowId)
            } else {
                chrome.tabs.create({url: url});
            }
        } catch (error) {}
    },
    filterObject(inputObj, PATTERN) {
        try {
            return Object.keys(inputObj)
                .filter(key => Object.keys(store).filter(str => {
                    return str.includes(PATTERN)
                }).includes(key))
                .reduce((obj, key) => {
                    obj[key] = inputObj[key];
                    return obj;
                }, {});
        } catch (e) {
            return {};
        }
    },
}