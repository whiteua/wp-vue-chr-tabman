import Vue from 'vue'
import App from './App'
import store from '../store'
import { BootstrapVue } from 'bootstrap-vue'
new Vue({
  el: '#app',
  store,
  render: h => h(App)
})
// Install BootstrapVue
Vue.use(BootstrapVue)
